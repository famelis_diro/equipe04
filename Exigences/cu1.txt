1. Cas d'utilisation #1
Cas d'utilisation : Création d'un compte membre.

But : Créer un fichier d'inscription dans le système qui représente un nouveau membre.

Acteur(s) : Membre
Acteur(s) secondaire(s) : Agent

Préconditions : Ne pas être déjà membre, le système est au menu principal.

Scénario principal :

1. Le système affiche une console avec plusieurs options : il faut taper une lettre pour choisir une option. (menu principal)

2. L'agent tape 'm' dans la console du système.

3. Le système propose trois choix concernant les comptes des membres : créer (c), modifier (m) ou supprimer (s).

4. L'agent entre 'c'.

5. La console invite l'agent à entrer les informations du membre une à une.

6. L'agent entre les informations personnelles du membre dans la console ainsi qu'un numéro unique arbitraire.
Il invite également le membre à s'acquitter des frais d'inscription, qui ne sont pas gérées par le logiciel.

6.1. Le système enregistre les informations. 

7. Le système affiche un récapitulatif des informations du membre dans la console, son numéro unique.

7.1. L'agent partage le numéro unique au membre.

8. L'agent entre n'importe quoi et appuie sur entrée pour retourner au menu de la console.

9. La console affiche le menu principal.

Scénario(s) alternatif(s) :

(Annuler la création d'un compte)

5a.1. Si le membre change d'avis, l'agent entre 'a' pour annuler.

5a.2. Le système affiche le menu principal de la console.

(Le membre ne peut pas payer les frais.)

5b.1. Si le membre ne peut pas acquitter les frais, l'agent entre 'n' pour signifier qu'il reviendra plus tard.

5b.2. Le système affiche le menu principal de la console.

(modifier un compte pour qu'il soit suspendu)

4a.1 L'agent entre 'p'.

4a.2 Le système demande si le membre a payé ses frais et d'entrer Oui (O) ou Non (N).

4a.3 L'agent entre Non.

4a.4 Le système modifie le compte du membre pour qu'il soit suspendu.

4a.5 Retour au point 9.

(modifier un compte changer la date de fin d'abonnement)

4b.1 L'agent entre 'p'.

4a.2 Le système demande si le membre a payé ses frais et d'entrer Oui (O) ou Non (N).

4b.3 L'agent entre Oui et entre la nouvelle date de fin d'abonnement.

4b.4 Le système change la date de fin d'abonnement dans le fichier du membre.

4b.5 Retour au point 9.

(supprimer un compte)

4c.1 L'agent entre 's'.

4c.2 Le système supprime le compte du membre des fichiers ainsi que tous les fichiers d'inscription à des services qui correspondaient au numéro unique du membre.

4c.3 Retour au point 9.

(modifier les infos personnelles d'un compte)

4d.1 L'agent entre 'd'.

4d.2 Le système demande les infos personnelles du membre.

4d.3 L'agent entre les nouvelles infos personnelles.

4d.4 Le système modifie le fichier du membre.

4d.5 Retour au point 9.

Postconditions : Un nouveau fichier de membre est enregistré dans le système et s'est fait assigné un numéro unique.