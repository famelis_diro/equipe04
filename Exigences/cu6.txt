+-------------------------------+
|							    |
|	  Cas d'utilisation #6	    |
|								|
+-------------------------------+

Cas d'utilisation : Consulter le nombre d'inscris d'une séance

But : Obtenir le nombre d'inscriptions pour un service à un moment souhaité.

Acteurs : Professionnel
Acteurs secondaires : Agent.

Préconditions : Le service doit avoir été enregistré et créé dans le système.

Scénario principal :
1. Le système affiche une console avec plusieurs options : il faut taper une
lettre pour choisir une option. (menu principal)
2. L'agent tape 'd' pour accéder au menu des requêtes de nombre d'inscrits.
3. La console demande à l'agent d'entrer un code de service existant pour
consulter son nombre d'inscriptions.
3.1. Le système vérifie si le code de service existe dans les fichiers
	des services enregistrés.
4. La console affiche le nombre d'inscrits pour le service et la capacité
maximale du service.
5. L'agent appuie sur entrée pour continuer.
5. La console affiche le menu principal.

Scénario(s) alternatif(s) :
	
(code de service entré invalide et choix de réessayer)
3a.1 Le code entré ne correspond à aucun code de service répertorié dans les
fichiers de service. 
3a.2 L'agent réessaye un nouveau code de service
3a.3 Le scénario reprends a l'étape 3.1

(code de service entré invalide et/ou choix de quitter)
3b.1 Le code entré ne correspond à aucun code de service répertorié dans les
fichiers de service. 
3b.2 L'agent tape 'a'
3b.3 Le scénario reprends a l'étape 5

	

Postconditions : La console a affiché le nombre d'inscrits pour le cours.