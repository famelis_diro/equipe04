package prototype;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Quentin
 * Classe principal du prototype. Contient le menu et les appels d'autre classes pour les 
 * differentes fonctionnalites.
 */
public class Gym {

	/**
	 * Scanner permettant de lire les entrees de l'agent.
	 */
	public static Scanner sc = new Scanner(System.in);

	/**
	 * Liste contenant les membres
	 */
	static ArrayList<Membre> tabMembre = new ArrayList<Membre>();

	/**
	 * Liste contenant les professionnels
	 */
	static ArrayList<Professionnel> tabProfessionnel = new ArrayList<Professionnel>(); 

	/**
	 * Liste contenant les services
	 */
	static ArrayList<Service> tabService = new ArrayList<Service>();

	/**
	 * Variable de type char qui servira tout au long du programme a stocker l'input de l'agent
	 */
	static char tache = ' ';

	/**
	 * Variable de type string qui servira a recuperer les donnees qu'entrera l'agent
	 */
	static String input;

	/**
	 * Fonction principale de #GYM qui affichera le menu et attendra la commande de l'agent
	 * @param args
	 */
	public static void main(String[] args) {				

		do {
			System.out.println(
					"+-------+\n" + 
							"| #GYM  |\n" + 
							"+-------+\n" +
							"\nQuelle tâche souhaitez vous exectuer? Tapez la touche correspondante.\n" + 
							"a) Afficher liste des services\n" +
							"c) Confirmation de presence a un service\n" +
							"d) Obtenir le nombre d'inscris a une seance\n" +
							"g) Generer enregistrement TEF\n" +
							"i) Inscription a une seance.\n" +
							"m) Gestion des dossiers de membre.\n" + 
							"p) Creation d'un nouveau professionnel.\n" +
							"s) Gestion des services.\n" +
							"q) Fermer le logiciel.\n" +
							"v) Validation d'entree.\n" + 
							"y) Generer le fichier de synthese"
					);

			tache = getCommand();
			if(tache != 'q') {
				executerTache(tache); 
				System.out.println("Appuyez sur la touche entree pour continuer.");
				input = sc.nextLine();
			}
		} while(tache != 'q');

		System.out.println("Au revoir!");

	}

	/**
	 * Fonction d'exection dediee a appeler la fonction correspondant a la tache entree par l'agent
	 */
	protected static void executerTache(char cmd) {

		switch(cmd) {
		case 'm': gestionClient();
		break;
		case 'p': tabProfessionnel.add(new Professionnel());
		break;
		case 'q': System.out.println("Au revoir!"); 
		break;
		case 'v': peutEntrer(); 
		break;
		case 's': gestionService();;
		break;
		case 'i': inscriptionService(); 
		break;
		case 'c': confirmerPresence();
		break;
		case 'd': nombreInscris();
		break;
		case 'a': afficherListeServices(); 
		break;
		case 'g': System.out.println(genererTEF());
		break;
		case 'y': System.out.println(genererSynthese());
		break;
		default: System.out.println("\nCeci n'est pas un caractere reconnu.\n");
		}
	}


	/**
	 * Permet de verifier que l'agent entre un seul charactere
	 * @return cmd est la commande de l'agent convertie de String -> char
	 */
	protected static char getCommand() {
		char cmd;
		boolean premierEssai = true;
		do {
			if(!premierEssai)
				System.out.println("Veuillez entrer un caractere valide:");
			premierEssai = false;
			input = sc.nextLine();
		} while(input.length() != 1);
		cmd = input.charAt(0);
		System.out.print("\n");
		return cmd;
	}

	/**
	 * Fonction de gestion des dossiers clients. Affiche le menu de gestion et attends l'entree
	 * de l'agent afin d'appeler la fonction correspondant a l'action voulue.
	 */
	protected static void gestionClient() {
		System.out.println("Souhaitez vous: \n" + 
				"c) Creer un nouveau dossier de membre\n" +
				"m) Modifier un dossier existant\n" + 
				"s) Supprimer un dossier existant\n"	+
				"Pressez n'importe quelle autre touche pour quitter."
				);
		tache = getCommand();
		switch(tache) {
		case 'c': tabMembre.add(new Membre()); 
		break;
		case 'm': modificationDossierMembre('m');
		break;
		case 's': modificationDossierMembre('s'); 
		break;
		case 'q': tache = ' '; break;
		default: break;
		}
	}

	/**
	 * Permet de modifier les informations liees a un membre ou de supprimer un dossier
	 * @param in correspond a l'entree de l'agent, a savoir 'm' pour une modification ou 's' pour une suprression
	 * La variable entiere posDossier correspond a la position du dossier de membre dans la liste des dossiers membre (-1 si inexistant)
	 */
	protected static void modificationDossierMembre(char in) {
		input = "";
		while(!numeroMembreValide(input) && !input.equals("q")) {
			System.out.println("Numero de dossier membre:");
			input = sc.nextLine();
		}
		if(!input.equals("q")) {
			int posDossier = estMembre(input);
			if(posDossier != -1) {
				if(in == 'm')
					tabMembre.get(posDossier).modifierDossier();
				else {
					tabMembre.remove(posDossier);
					System.out.println("Dossier supprime.");
				}
			}
			else 
				System.out.println("Ce numero ne corresond pas a un membre actuel.");
		}
		else
			System.out.println("\nOperation annulee");
	}

	/**
	 * Fonction de gestion des services. Affiche le menu de gestion et attends l'entree
	 * de l'agent afin d'appeler la fonction correspondant a l'action voulue.
	 */
	protected static void gestionService() {
		System.out.println("Souhaitez vous: \n" + 
				"c) Creer un nouveau service\n" +
				"m) Modifier un service existant\n" + 
				"s) Supprimer un service existant\n"	+
				"Pressez n'importe quelle autre touche pour quitter."
				);
		tache = getCommand();
		switch(tache) {
		case 'c': creationService(); 
		break;
		case 'm': modificationService('m');
		break;
		case 's': modificationService('s'); 
		break;
		case 'q': tache = ' '; 
		break;
		default: break;
		}
	}

	/**
	 * Permet de modifier la capacite maximum d'un service ou de le supprimer
	 * @param in correspond a l'entree de l'agent, a savoir 'm' pour une modification ou 's' pour une suprression
	 * La variable entiere posService correspond a la position du service dans la liste des services (-1 si inexistant)
	 */
	protected static void modificationService(char in) {
		input = "";
		while(!numeroServiceValide(input)) {
			System.out.println("Numero de service:");
			input = sc.nextLine();
		}
		int posService = serviceExistant(input); 
		if(posService != -1) {
			if(in == 'm')
				tabService.get(posService).modificationService();
			else {
				tabService.remove(posService);
				System.out.println("Service supprime.");
			}
		}
		else 
			System.out.println("Ce numero ne corresond pas a un service.");
	}

	/**
	 * Permet de verifier si un membre est dans le inscrit, sans se soucier des frais
	 * @param num le numero de membre
	 * @return La position du dossier membre dans la liste des dossiers, -1 si inexsistant
	 */
	protected static int estMembre(String num) {
		for(int i=0; i<tabMembre.size(); i++) {
			if(num.equals(tabMembre.get(i).getNumero())) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Permet de verifier le statut lie a un numero de membre
	 * @param num le numero de membre
	 * @return 'v' si le membre est valide (existant et qui a paye ses frais), 's' si le membre est suspendu (existant mais frais impayes) 
	 * ou 'n' si le membre n'existe pas
	 */
	protected static char estMembreActuel(String num) {
		int posDossier = estMembre(num);
		if(posDossier != -1)
			if(tabMembre.get(posDossier).aPayer()) 
				return('v');

			else
				return('s');

		return 'n';
	}

	/**
	 * Permet de verifier si un numero correspond a un numero de professionnel
	 * @param num le numero de professionnel
	 * @return true si le numero est lie a un compte professionnel, sinon false
	 */
	protected static int estProfessionnel(String num) {
		for(int i=0; i<tabProfessionnel.size(); i++) {
			if(num.equals(tabProfessionnel.get(i).getNumero())) {
				return i;
			}
		}
		return -1;
	}


	/**
	 * Permet de savoir si un membre peut entrer au gymnase en affichant un message avec le statut correspondant
	 */
	protected static void peutEntrer() {
		System.out.println("Quel est le numero d'adherent?");
		input = sc.nextLine();
		char estMembre = estMembreActuel(input);
		if(estMembre == 'v') 
			System.out.println("Valide!");
		else if(estMembre == 's')
			System.out.println("Ce membre est suspendu");
		else
			if(estProfessionnel(input) != -1)
				System.out.println("Valide");
			else
				System.out.println("Numero invalide");
	}

	/**
	 * Permet de creer un nouveau service et l'ajouter a la liste des services disponibles
	 * @return Un message correspondant au statut de la creation
	 */
	protected static String creationService() {
		boolean continuer = true;
		Professionnel proOffrantService;
		Service nouveauService;
		int posPro;
		do {
			System.out.println("Numero de professionnel:");
			input = sc.nextLine();
			if(input.equals("q")) // Permet de quitter la fonction si l'agent entre 'q'
				continuer = false;
			posPro = estProfessionnel(input);
		} while (!input.equals("q") && (posPro == -1));
		if(!continuer)
			return("Annulation creation service");
		else {
			nouveauService = new Service(input);
			proOffrantService = tabProfessionnel.get(posPro);
			proOffrantService.ajouterServiceOffert(nouveauService);
			tabService.add(nouveauService);
			return ("Service cree! \n");
		}
	}

	/**
	 * Permet d'inscrire un membre a un service
	 * @return Un String qui s'imprimera a l'ecran indiquant le statut lie a l'inscription
	 */
	protected static void inscriptionService() {
		do {
			System.out.println("Code du service auquel vous souhaitez vous inscrire:");
			input = sc.nextLine();
		} while (!numeroServiceValide(input) && !input.equals("a")); 
		if(!input.equals("a")) {
			if(serviceExistant(input) != -1) {
				tabService.get(serviceExistant(input)).inscription();
			}
			else {
				System.out.println("Ce service n'existe pas.");
			}
		}
		else
			System.out.println("Inscription annulee\n");
	}

	/**
	 * @param num un numero de membre
	 * @return true si le numero correspond a un numero de membre ([0,1,2,3,4] suivi de 8 chiffres), sinon false
	 */
	protected static boolean numeroMembreValide(String num) {
		if(num.length() == 9) 
			if(num.matches("[0-4]\\d{8}")) {
				return true;
			}
		return false;
	}

	/**
	 * @param num un numero de service
	 * @return true si le numero correspond a un numero de service ([5,6,7,8,9] suivi de 8 chiffres), sinon false
	 */
	protected static boolean numeroServiceValide(String num) {
		if(num.length() == 7) 
			if(num.matches("\\d{7}")) {
				return true;
			}
		return false;
	}

	/**
	 * @param numService un numero de service
	 * @return La position de service dans la liste des services, -1 si le service n'est pas existant
	 */
	protected static int serviceExistant(String numService) {
		for(int i=0; i<tabService.size(); i++) {
			if(tabService.get(i).getNumService().equals(numService))
				return i;
		}
		return -1;
	}

	/**
	 *  Permet de confirmer la presence d'un membre a une seance. Imprime un message a l'ecran correspondant au statut de l'inscription.
	 */
	protected static void confirmerPresence() {
		input = "";
		int emplacementServiceList = serviceExistant(input);
		while(emplacementServiceList < 0 && !input.equals("q")) {
			System.out.println("Code du service auquel vous souhaitez confirmez la presence:");
			input = sc.nextLine(); 
		}
		if(input.equals("q"))
			System.out.println("Confirmation annulee.");
		else {
			tabService.get(emplacementServiceList).confirmerPresenceService();
			System.out.println("C'est confirme!");
		}
	}

	/**
	 * Permet d'afficher le nombre de personnes inscris a une seance, mais offre aussi la possibilite d'afficher la liste des inscris
	 */
	protected static void nombreInscris() {
		input = "";
		if(input.length()>0 && !input.equals("a")) {
			int emplacementServiceList = serviceExistant(input);
			while(emplacementServiceList < 0) {
				System.out.println("Code du service dont vous voulez les informations:");
				input = sc.nextLine(); 
				emplacementServiceList = serviceExistant(input);
			}
			System.out.println("Il y a actuellement " + tabService.get(emplacementServiceList).getNbrInscris() + " personnes inscrient a cette seance"
					+ " sur une capacitee maximale de " + tabService.get(emplacementServiceList).getCapaciteMax()  + ".\n"
					+ "Voulez vous plus d'informations? (liste des inscris) o/n");
			input = sc.nextLine();
			if(input.charAt(0) == 'o') {
				tabService.get(emplacementServiceList).afficherInfosInscris();
			}		
		}
		else
			System.out.println("Operation annulee.");
	}

	/**
	 * Permet d'afficher la liste des services existant
	 */
	protected static void afficherListeServices() {
		if(tabService.size()==0)
			System.out.println("Aucun service n'est disponible actuellement.\n");
		else 
			for(int i=0; i<tabService.size(); i++)
				System.out.println(tabService.get(i));
	}

	/**
	 * Permet de generer le fichier TEF 
	 */
	protected static String genererTEF() {
		String textRetour = "";
		Professionnel proActuel;
		for(int i=0; i<tabProfessionnel.size(); i++) {
			proActuel = tabProfessionnel.get(i);
			textRetour += ( "Professionnel #" + i +" :\n"
					+ "Nom et prenom: " + proActuel.getNom() + " " + proActuel.getPrenom() + "\n" 
					+ "Numero: " + proActuel.getNumero() + "\n"
					+ "Liste des services offert par ce professionnel: \n" + proActuel.getListServices() + "\n\n"
					);
		}
		return textRetour;
	}

	/**
	 * Permet de retourner le fichier de synthese
	 */
	protected static String genererSynthese() {
		String textRetour = "";
		int nbrProfessionnels = tabProfessionnel.size();
		int nbrServicesTotal = tabService.size();
		double salaire = 0;
		double fraisTotal = 0;
		Professionnel proActuel;
		for(int i=0; i<nbrProfessionnels; i++) {
			proActuel = tabProfessionnel.get(i);
			salaire = proActuel.getSalaire();
			fraisTotal += salaire;
			textRetour += ( "Le professionel: " + proActuel.getPrenom() + " " + proActuel.getNom() +
					", professionnel numero " + proActuel.getNumero() + " a fourni " + proActuel.getNbrServicesFourni() +
					" service(s) et doit ainsi etre paye " + salaire + "$\n"); 
		}
		textRetour += "\nCela fait donc un total de " + nbrProfessionnels + " professionnels ayant fourni "
				+ nbrServicesTotal + " services et donc des frais totaux de " + fraisTotal + "$ a payer.";
		return textRetour;
	}

	/**
	 * @param in Un String que l'on veut comparer a une date
	 * @return True si c'est une date valide, false sinon
	 */
	protected boolean isADate(String in) {
		return in.matches("([0-2]\\d|[3][0-1])[-]([0][0-9]|[1][0-2])[-]\\d{4}");
	}

	/**
	 * @param in Un String que l'on veut comparer un temps de la forme HH:MM:SS
	 * @return True si c'est le bon format, false sinon
	 */
	protected boolean isTimeSecond(String in) {
		return in.matches("([0-1]\\d|[2][0-4])([:][0-5]\\d){2}");
	}

	/**
	 * @param in Un String que l'on veut comparer un temps de la forme HH:MM
	 * @return True si c'est le bon format, false sinon
	 */
	protected boolean isTime(String in) {
		return in.matches("([0-1]\\d|[2][0-4])([:][0-5]\\d)");
	}



}
