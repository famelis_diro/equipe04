package prototype;
import java.util.ArrayList;

/**
 * @author Quentin WOLAK
 * Classe correspondant a un membre
 */
public class Membre extends Personnel {

	/**
	 * Variable static a la classe de type liste qui contiendra l'ensemble des numeros de membre afin de ne jamais generer 2x le même.
	 */
	static ArrayList<Integer> tabNumeros = new ArrayList<Integer>();
	
	/**
	 * Variable boolean qui contiendra true si le membre a paye son engagement, false sinon
	 */
	private boolean paye = false;
	
	/**
	 * Variable boolean qui contiendra true si le membre a deja un numero de membre, false sinon
	 */
	private boolean aDejaNumero = false;
	
	/**
	 * Chaine de characteres qui contiendra la date de fin d'engagement du membre
	 */
	private String dateFinEngagement;

	/**
	 * Constructeur de la classe membre
	 */
	public Membre() {
		if(!this.stop) {
			payer();
			System.out.println("Recapitulatif:\n" + this);
		}
	}

	/**
	 * Permet de modifier le dossier d'un membre en traversant tous les champs incluant le statut d'engagement
	 */
	public void modifierDossier() {
		char tache ='a';
		while(tache != 'p' && tache != 'd') {
			System.out.println("Souhaitez vous p) changer le statut d'engagement ou d) modifier les donnees personnels?");
			tache = getCommand();
		}
		if(tache == 'p') {
			this.payer();
		}
		else if(tache == 'd') {
			System.out.println("Si vous ne souhaitez pas modifier le champs actuel, pressez entree.");
			System.out.println("Nom: (Actuel: " + this.nom + " )");
			input = sc.nextLine();
			if(input != "")
				this.nom = input;
			System.out.println("Prenom: (Actuel: " + this.prenom + " )");
			input = sc.nextLine();
			if(input != "")
				this.prenom = input;
			System.out.println("Code postal: (Actuel: " + this.codePostal + " )");
			input = sc.nextLine();
			if(input != "")
				this.nom = input;
			System.out.println("Numero de telephone: (Actuel " + this.numeroDeTelephone + " )");
			input = sc.nextLine();
			if(input!="")
				this.numeroDeTelephone = input;
			System.out.println("Date de naissance: (Actuel: " + this.dateDeNaissance + " )");
			input = sc.nextLine();
			if(input != "")
				this.nom = input;
		}
		System.out.println("Donnees mises a jour: \n" + this);
	}

	/**
	 * Reecriture de la fonction adaptee a la creation d'un numero de membre, ce qui signifie que le numero commencera par 
	 * 0,1,2,3 ou 4
	 */
	@Override
	protected void genererNumeroPersonnel() {
		if(!aDejaNumero) {
			int numero;
			do {
				numero = (int) (Math.random()*100000000);
			} while(!numeroUnique(numero));
			tabNumeros.add(numero);
			this.numeroPersonnel = "" + numero;
			while(numeroPersonnel.length() < 8)
				this.numeroPersonnel = "0"+this.numeroPersonnel;
			numero = (int) (Math.random()*5);
			this.numeroPersonnel = "" + numero + this.numeroPersonnel;
			this.aDejaNumero = true;
			System.out.println("\nLe numero personnel est: " + this.numeroPersonnel + "\n");
		}
		else 
			System.out.println("\nLe numero personnel est toujours: " + this.numeroPersonnel + "\n");
	}

	/**
	 * Permet de comparer si le numero est bien unique (n'a pas encore ete assigne)
	 * @return true si le numero est unique, false sinon
	 */
	protected boolean numeroUnique(int num) {
		for(int i=0; i<tabNumeros.size(); i++) {
			if(num == tabNumeros.get(i))
				return false;
		}
		return true;
	}

	/**
	 * Permet de changer le statut d'engagement d'un membre en paye ou impaye et de modifier la date de fin d'engagement
	 */
	public void payer() {
		char answer = 'a';
		while(answer != 'o' && answer != 'n') {
			System.out.println("Les frais sont-ils paye? o/n");
			input = sc.nextLine();
			if(input.length()>0)
				answer = input.charAt(0);
		}
		if(answer == 'o') {
			this.paye = true;
			input="";
			while(!isADate(input)) {
				System.out.println("Date de fin d'engagement: ");
				input = sc.nextLine();
			};
			this.dateFinEngagement = input;
			genererNumeroPersonnel();
		}
		else {
			this.paye = false;
			System.out.println("Pensez a payer.\n");
		};
	}

	/**
	 * Permet de savoir le statut d'engagemet d'un membre
	 * @return true si le membre a paye ses frais, false sinon
	 */
	protected boolean aPayer() {
		return paye;
	}
	
	/**
	 * Reecriture de la fonction toString afin de correspondre a l'impression de donnees d'un membre
	 */
	@Override
	public String toString() {
		if(this.paye == true)
			return (super.toString() + "Date de fin d'engagement: " + this.dateFinEngagement + "\n");
		else
			return (super.toString() + "Date de fin d'engagement: Non engage \n");
	}


}
