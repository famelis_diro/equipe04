package prototype;
/**
 * @author Quentin
 * Classe correspondant aux participants d'une seance
 */
public class Participant {

	/**
	 * La date d'inscription du participant
	 */
	private String dateActuelle;
	
	/**
	 * L'heure d'inscription du participant
	 */
	
	private String heureActuelle;
	/**
	 * La date de la seance de service a la quelle le membre va participer
	 */
	private String dateService;
	
	/**
	 * Le numero du professionnel qui donne la seance
	 */
	private String numPro;
	
	/**
	 * Le numero de membre du participant
	 */
	private String numMembre;
	
	/**
	 * Le code du service qui sera donne pendant la seance
	 */
	private String codeService;
	
	/**
	 * Commentaire facultatif sur l'inscription du membre a la seance
	 */
	private String commentaire;
	
	/**
	 * @param dateActu
	 * @param heureActu
	 * @param dateServ
	 * @param numPr
	 * @param numMe
	 * @param codeSe
	 * @param comm
	 */
	public Participant(String dateActu, String heureActu, String dateServ, String numPr, String numMe, String codeSe, String comm) {
		this.dateActuelle = dateActu;
		this.heureActuelle = heureActu;
		this.dateService = dateActuelle;
		this.numPro = numPr;
		this.numMembre = numMe;
		this.codeService = codeSe;
		this.commentaire = comm;
		
	}

	/**
	 * @param dateActu
	 * @param heureActu
	 * @param comm
	 * @return un string correspondant a un resume de l'inscription
	 */
	public String confirmerPresence(String dateActu, String heureActu, String comm) {
		return("Date et heure actuelles: "+ dateActu + " " + heureActu +
				"\nNumero du professionnel: " + this.numPro +
				"\nNumero du membre: " + this.numMembre +
				"\nCode du service: " + this.codeService +
				"\nCommentaire: " + comm +
				"\n");
	}
	
	/**
	 * @return le numero de membre du participant
	 */
	public String getNumMembre() {
		return this.numMembre;
	}

	/**
	 * Reecriture de la fonction toString pour correspondre a l'enregistrement d'un participant
	 */
	public String toString() {
		return("Date et heure actuelles: "+ this.dateActuelle + " " + this.heureActuelle +
				"\nDate du service: "+ this.dateService +
				"\nNumero du professionnel: " + this.numPro +
				"\nNumero du membre: " + this.numMembre +
				"\nCode du service: " + this.codeService +
				"\nCommentaire: " + this.commentaire);
	}
	
}
