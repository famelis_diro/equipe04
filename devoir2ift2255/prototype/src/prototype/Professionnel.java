package prototype;
import java.util.ArrayList;

/**
 * @author Quentin
 * Classe correspondant a un professionnel
 */
public class Professionnel extends Personnel {

	/**
	 * Variable static a la classe de type liste qui contiendra l'ensemble des numeros de personnel afin de ne jamais generer 2x le même.
	 */
	static ArrayList<Integer> tabNumeros = new ArrayList<Integer>();

	protected ArrayList<Service> tabServiceOffert = new ArrayList<Service>();

	/**
	 * Constructeur du professionnel, heritant du constructeur professionnel + generation du numero personnel d'un professionnel
	 */
	public Professionnel() {
		if(!this.stop) {
			genererNumeroPersonnel();
			System.out.println("Recapitulatif:\n" + this);
		}
	}

	/**
	 * Permet de comparer si le numero est bien unique (n'a pas encore ete assigne)
	 * @return true si le numero est unique, false sinon
	 */
	protected boolean numeroUnique(int num) {
		for(int i=0; i<tabNumeros.size(); i++) {
			if(num == tabNumeros.get(i))
				return true;
		}
		return false;
	}

	/**
	 * Reecriture de la fonction adaptee a la creation d'un numero de professionnel, ce qui signifie que le numero commencera par 
	 * 5,6,7,8 ou 9
	 */
	@Override
	protected void genererNumeroPersonnel() {
		int numero;
		do {
			numero = (int) (Math.random()*100000000);
		} while(numeroUnique(numero));
		tabNumeros.add(numero);
		this.numeroPersonnel = "" + numero;
		while(numeroPersonnel.length() < 8)
			this.numeroPersonnel = "0"+this.numeroPersonnel;
		numero = (int) ((Math.random()*5) + 5);
		this.numeroPersonnel = "" + numero + this.numeroPersonnel;
		System.out.println("Le numero personnel est: " + this.numeroPersonnel);
	}

	/**
	 * Permet d'ajouter le service a la liste de services offert par ce professionnel
	 * @param serv le service qu'offre le professionnel
	 */
	public void ajouterServiceOffert(Service serv) {
		this.tabServiceOffert.add(serv);
	}

	public int getNbrServicesFourni() {
		return this.tabServiceOffert.size();
	}

	/**
	 * Multiplie le tarif de chaque services par le nombre de clients et somme le tout 
	 * @return le salaire de l'employee en supposant qu'il gagne 60% des frais 
	 */
	public double getSalaire() {
		double salaire = 0;
		for(int i=0; i<tabServiceOffert.size(); i++)
			salaire += tabServiceOffert.get(i).getTarif()*tabServiceOffert.get(i).getNbrInscris();
		salaire = 0.6*salaire;
		return salaire;
	}


	/**
	 * @return la liste des services offert par un professionnel ainsi que le nombre de participants
	 */
	public String getListServices() {
		String result = "";
		if(this.tabServiceOffert.size() == 0)
			return "Aucun service actuellement offert par ce professionnel.";
		else {
			for(int i=0; i<this.tabServiceOffert.size(); i++) {
				result = result + (i+1) + ". Service numero: " + this.tabServiceOffert.get(i).getNumService() + " ayant " + this.tabServiceOffert.get(i).getNbrInscris() + " participants inscrits\n";
			}
			return result;
		}
	}

}
