package prototype;
/**
 * @author Quentin
 *
 */
public class Service extends Gym {

	/**
	 * Numero du professionnel qui donnera le service
	 */
	protected String numeroProfessionnel;
	
	/**
	 * Code du service
	 */
	protected String code = "";
	
	/**
	 * Date de creation du service
	 */
	protected String dateActuelle;
	
	/**
	 * Heure de creation du service
	 */
	protected String heureActuelle;
	
	/**
	 * Date de debut du service
	 */
	protected String dateDebut;
	
	/**
	 * Date de fin du service
	 */
	protected String dateFin;
	
	/**
	 * Heure a la quelle commencera chaque seance du service
	 */
	protected String heureDebut;
	
	/**
	 * Reccurence des seances du service
	 */
	protected String reccurence;
	
	/**
	 * Nombre maximum de participants au service
	 */
	protected int capaciteMax = 31;
	
	/**
	 * Frais de participation au service
	 */
	protected double fraisDeService = 101;
	
	/**
	 * Commentaire associee au service
	 */
	protected String commentaire = "Aucun commentaire.";
	
	/**
	 * Liste contenant les membres participants a une seance
	 */
	protected Participant listeMembreSeance[];
	
	/**
	 * Nombre d'inscrits au service
	 */
	protected int nbrInscrit = 0;
	
	/**
	 * Liste contenant les numeros de membre des participants ayant valides leurs presence au service
	 */
	protected String tabConfirmationPresence[];

	/**
	 * @param numeroPro Le numero du professionnel qui donnera le service
	 * Le constructeur lance la fonction de creation d'un service
	 */
	public Service (String numeroPro) {
		this.numeroProfessionnel = numeroPro;
		nouveauService();
	}

	/**
	 * Fonction de creation d'un service. Affiche tous les champs a completer par l'agent a l'ecran et enregistre les donnees.
	 */
	private void nouveauService() {
		System.out.println("Code de service a 7 chiffres: ");
		input = sc.nextLine();
		while(input.length() != 7 || !(serviceExistant(input) == -1)) {
			System.out.println("Ce service existe deja ou n'est pas un numero valide, choisissez un autre code:");
			input = sc.nextLine();
		}
		this.code = input;
		while(!isADate(input)) {
			System.out.println("Date actuelle: (JJ-MM-AAAA)");
			input = sc.nextLine();
		}
		this.dateActuelle = input;
		while(!isTimeSecond(input)) {
			System.out.println("Heure actuelle: (HH:MM:SS)");
			input = sc.nextLine();
		}
		this.heureActuelle = input;
		while(!isADate(input)) {
			System.out.println("Date de debut de service: (JJ-MM-AAAA)");
			input = sc.nextLine();
		}
		this.dateDebut = input;
		input = "";
		while(!isADate(input)) {
			System.out.println("Date de fin de service: (JJ-MM-AAAA)");
			input = sc.nextLine();
		}
		this.dateFin = input;
		while(!isTime(input)) {
			System.out.println("Heure du service: (HH:MM)");
			input = sc.nextLine();
		}
		this.heureDebut = input;
		System.out.println("Reccurence du service: (Entrez les jours a la suite)");
		this.reccurence = sc.nextLine();
		while(capaciteMax>30 || capaciteMax<0) {
			System.out.println("Capacite maximum de personne: (Max 30)");
			input = sc.nextLine();
			if(input.matches("\\d{1,2}"))
				capaciteMax = Integer.parseInt(input);
		}
		listeMembreSeance = new Participant[capaciteMax];
		tabConfirmationPresence = new String[capaciteMax];
		while(fraisDeService>100 || fraisDeService<0) {
			System.out.println("Frais de service: (Max 100.00$, pensez a mettre '.' et non ',')");
			input = sc.nextLine();
			if(input.matches("\\d{1,3}")) 
				fraisDeService = Double.parseDouble(input);
		}
		System.out.println("Commentaire: (Pressez entree pour passer)");
		input = sc.nextLine();
		if(input.length()>0) 
			this.commentaire = input;
		System.out.println("Resume du service:\n" + this);
	}

	/**
	 * Fonction permettant de modifier le nombre de participants maximum a un service ou bien de supprimer un service.
	 * Si on ne veut pas modifier, on presse juste entree
	 */
	public void modificationService() {
		System.out.println("Nouveau nombre de participants maximum: (Actuel: " + this.capaciteMax + " )");
		input = sc.nextLine();
		if(input != "") {
			if(input.matches("\\d{1,2}"))
				capaciteMax = Integer.parseInt(input);
				while(capaciteMax>30 || capaciteMax<0) {
					System.out.println("Capacite maximum de personne: (Max 30)");
					input = sc.nextLine();
					if(input.matches("\\d{1,2}"))
						capaciteMax = Integer.parseInt(input);
				}
				System.out.println("La capacite maximale a ete modifiee.\n"
						+ "Recapitulatif:\n" + this);
		}
	}
	
	/**
	 * Fonction permettant d'inscrire un membre a un service
	 * Imprime un message a l'ecran affichant le statut de l'inscriptione
	 */
	public void inscription() {
		System.out.println("Vous allez actuellement vous inscrire a ce service: \n\n" + this);
		System.out.println("Numero de membre pour inscription a une seance: ");
		input = sc.nextLine();
		if(estMembreActuel(input) == 'v') {
			if(nbrInscrit < listeMembreSeance.length) {
				if(!estDejaInscrit(input)) {
					System.out.println("Commentaire: (Pressez entree pour passer)");
					String commentaireInscription = sc.nextLine();
					listeMembreSeance[nbrInscrit] = new Participant(this.dateActuelle, this.heureActuelle, this.dateDebut, this.numeroProfessionnel, input, this.code, commentaireInscription);
					tabConfirmationPresence[nbrInscrit] = "Participant " + input + " non confirme.";
					nbrInscrit++;
					System.out.println("Inscription validee.");
				}
				else
					System.out.println ("Membre deja inscrit pour cette seance.");
			}
			else
				System.out.println("Ce cours est complet.");
		}
		else if(estMembreActuel(input) == 's')
			System.out.println ("Membre suspendu.");
		else
			System.out.println ("Ceci n'est pas un numero de membre existant.");

	}

	/**
	 * @param num le numero de membre dont on veut tester s'il est deja inscrit a la seance
	 * @return true si le membre est deja inscrit, false sinon
	 */
	private boolean estDejaInscrit(String num) {
		for(int i=0; i<nbrInscrit; i++) {
			if(listeMembreSeance[i].getNumMembre().equals(num))
				return true;
		}
		return false;
	}

	/**
	 * Sert a confirmer la presence d'un membre a une seance
	 */
	public void confirmerPresenceService() {
		while(!numeroMembreValide(input) && input.equals("a")) {
			System.out.println("Numero du membre: ");
			input = sc.nextLine();
		}
		if(!input.equals("a")) {
			for(int i=0; i<nbrInscrit; i++) {
				if(listeMembreSeance[i].getNumMembre().equals(input)) {
					String tempDate = " ";
					while(!isADate(tempDate)) { 
						System.out.println("Quelle est la date actuelle?");
						tempDate = sc.nextLine();
					}
					String tempTime = " ";
					while(!isTimeSecond(tempTime)) { 
						System.out.println("Quelle est l'heure actuelle?");
						tempTime = sc.nextLine();
					}
					System.out.println("Commentaire: (Pressez entree pour passer) ");
					String tempComm = sc.nextLine();
					tabConfirmationPresence[i] = listeMembreSeance[i].confirmerPresence(tempDate, tempTime, tempComm);
				}
			}
		}
		else
			System.out.println("Confirmation annulee.");
	}

	/**
	 * Permet d'afficher les informations sur les membres participants a un service
	 */
	public void afficherInfosInscris() {
		if(this.nbrInscrit == 0)
			System.out.println("Il n'y a aucun inscrit a cette seance");
		else
		for(int i=0; i<this.nbrInscrit; i++)
			System.out.println(listeMembreSeance[i]);
	}

	/**
	 * @return le numero du service 
	 */
	public String getNumService() {
		return this.code;
	}

	/**
	 * @return la capacite maximal d'inscription au service
	 */
	public int getCapaciteMax () {
		return capaciteMax;
	}

	/**
	 * @return le nombre de personnes inscrit au service
	 */
	public int getNbrInscris () {
		return nbrInscrit;
	}
	
	/**
	 * @return le tarif du service pour une personne
	 */
	public double getTarif() {
		return this.fraisDeService;
	}

	/**
	 * Reecriture de la fonction toString pour correspondre aux donnees a enregistrer d'un service
	 */
	public String toString() {
		return("Date et heure actuelles: "+ this.dateActuelle + " " + this.heureActuelle +
				"\nDate de debut du service: "+ this.dateDebut +
				"\nDate de fin du service: " + this.dateFin +
				"\nHeure du service: " + this.heureDebut +
				"\nReccurence hebdomadaire du service: " + this.reccurence +
				"\nCapacite maximale: " + this.capaciteMax +
				"\nNombre d'inscris: " + this.nbrInscrit +
				"\nNumero du professionnel: " + this.numeroProfessionnel +
				"\nCode du service: " + this.code +
				"\nFrais du service: " + this.fraisDeService +
				"\nCommentaire: " + this.commentaire + "\n");
	}
}
