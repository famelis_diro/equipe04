package prototype;

import java.util.Scanner;

/**
 * @author Quentin
 * Classe correspondante au personnel humain, englobe donc les membres ainsi que les professionnels
 */
public abstract class Personnel{
	
	/**
	 * Scanner permettant de lire les entrees de l'agent.
	 */
	public static Scanner sc = new Scanner(System.in);
	
	/**
	 * Variable de type string qui servira a recuperer les donnees qu'entrera l'agent
	 */
	protected static String input;

	/**
	 * Nom du membre/professionnel
	 */
	protected String nom;
	
	/**
	 * Prenom du membre/professionnel
	 */
	protected String prenom;
	
	/**
	 * Code postal du membre/professionnel
	 */
	protected String codePostal;
	
	/**
	 * Date de naissance du membre/professionnel
	 */
	protected String dateDeNaissance;
	
	/**
	 * Numero de telepgone du membre/professionnel
	 */
	protected String numeroDeTelephone;
	
	/**
	 * Boolean servant a indiquer si la creation doit continuer ou pas
	 */
	protected boolean stop = false;
	
	/**
	 * Numero personnel du membre/professionnel
	 */
	protected String numeroPersonnel = "Pas encore assigne.";
	
	/**
	 * Constructeur de la classe personnel servant a entrer les donnees communes aux deux classes heritantes 
	 */
	public Personnel() {
		System.out.println("Nom:");
		this.nom = getEntry();
		if(!stop) {
			System.out.println("Prenom:");
			this.prenom = getEntry(); 
		}
		if(!stop) {
			System.out.println("Code postal:");
			this.codePostal = getEntry(); 
		}
		if(!stop) {
			System.out.println("Numero de telephone: ");
			this.numeroDeTelephone = getEntry();
		}
		if(!stop) {
			do {
				System.out.println("Date de naissance: (format: JJ-MM-AAAA)");
				input = getEntry();
			} while(!isADate(input));
			this.dateDeNaissance = input;
		}
	}
	
	/**
	 * Permet de verifier la validitee de la syntaxe de informations entrees par l'agent
	 * @return l'entree de l'agent si elle est valide
	 */
	protected String getEntry () {
		String answer = sc.nextLine();
		while(answer.length()<=0) {
			System.out.println("Veuillez reessayer:");
			answer = sc.nextLine();
		}
		if(answer.charAt(0) == 'a' && answer.length() == 1) {
			stop = true;
			System.out.println("Creation annulee.\n");
		};
	
		return answer;
	}
	
	/**
	 * Permet de verifier que l'agent entre un seul charactere
	 * @return cmd est la commande de l'agent convertie de String -> char
	 */
	protected static char getCommand() {
		char cmd;
		boolean premierEssai = true;
		do {
			if(!premierEssai)
				System.out.println("Veuillez entrer un caractere valide:");
			premierEssai = false;
			input = sc.nextLine();
		} while(input.length() != 1);
		cmd = input.charAt(0);
		System.out.print("\n");
		return cmd;
	}


	/**
	 * Fonction permettant de generer un numero personel. Elle sera defini dans les classes heritantes
	 */
	protected abstract void genererNumeroPersonnel();
	
	/**
	 * Fonction qui sera redefini dans les classes heritantes
	 * @param num le numero de personnel dont on veut verifier l'unicite
	 * @return True si unique, false sinon
	 */
	protected abstract boolean numeroUnique(int num);

	/**
	 * @return le numero personnel
	 */
	public String getNumero() {
		return this.numeroPersonnel;
	}

	/**
	 * @return le nom de la personne
	 */
	public String getNom() {
		return this.nom;
	}
	
	public String getPrenom() {
		return this.prenom;
	}
	
	/**
	 * @param in Un String que l'on veut comparer a une date
	 * @return True si c'est une date valide, false sinon
	 */
	protected boolean isADate(String in) {
		return in.matches("([0-2]\\d|[3][0-1])[-]([0][0-9]|[1][0-2])[-]\\d{4}");
	}

	/**
	 * Reecriture de la fonction toString pour correspondre aux donnees a enregistrer d'un personnel en general
	 */
	public String toString() {
		return("Nom: " + this.nom
				+ "\nPrenom: " + this.prenom
				+ "\nCode postal: " + this.codePostal
				+ "\nDate de naissance: " + this.dateDeNaissance
				+ "\nNumero de telephone: " + this.numeroDeTelephone
				+ "\nNumero personnel: " + this.numeroPersonnel + "\n"
				);
	}

};





